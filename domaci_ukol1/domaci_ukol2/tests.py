import unittest
from fridge import *


class TestFridge(unittest.TestCase):

    def test_add_food(self):
        self.assertTrue(add_food({'name': 'blue cheese', 'price': 40, 'expired': False})) # Return True, pokud expired == False
        self.assertFalse(add_food({'name': 'blue cheese', 'price': 40, 'expired': True})) # Return False, pokud expired == True
        self.assertIn({'name': 'blue cheese', 'price': 40, 'expired': False}, fridge) # Test, jestli byla polozka pridana, pokud mela byt


    def test_add_foods(self):
        self.assertTrue(add_foods({'name': 'ham', 'price': 30, 'expired': False},
                                           {'name': 'salad', 'price': 45, 'expired': False},
                                           {'name': 'egg', 'price': 4, 'expired': False},
                                           {'name': 'wine', 'price': 200, 'expired': False},
                                           {'name': 'orange juice', 'price': 40, 'expired': False})) # Return True, pokud vsechny expired == False
        self.assertIn({'name': 'ham', 'price': 30, 'expired': False}, fridge)
        self.assertIn({'name': 'salad', 'price': 45, 'expired': False}, fridge)
        self.assertIn({'name': 'egg', 'price': 4, 'expired': False}, fridge)
        self.assertIn({'name': 'wine', 'price': 200, 'expired': False}, fridge)
        self.assertIn({'name': 'orange juice', 'price': 40, 'expired': False}, fridge)  # Test, zda byly vsechny polozky pridany
        self.assertFalse(add_foods({'name': 'ham', 'price': 60, 'expired': False},
                                           {'name': 'salad', 'price': 90, 'expired': True},
                                           {'name': 'egg', 'price': 8, 'expired': True},
                                           {'name': 'wine', 'price': 400, 'expired': False},
                                           {'name': 'orange juice', 'price': 80, 'expired': False})) # Return False, jestli nektera expired == True
        self.assertIn({'name': 'ham', 'price': 60, 'expired': False}, fridge)
        self.assertIn({'name': 'wine', 'price': 400, 'expired': False}, fridge)
        self.assertIn({'name': 'orange juice', 'price': 80, 'expired': False}, fridge)  # Test, zda byly spravne polozky pridany


    def test_eat_food(self):
        self.assertTrue(eat_food("ham")) # Return True, pokud je polozka v seznamu
        self.assertNotIn({'name': 'ham', 'price': 30, 'expired': False}, fridge) # Test, zda byla polozka odebrana
        self.assertEqual(eat_food("poop"), "poop" in [i["name"] for i in fridge])


    def test_get_price(self):
        price = 0
        for i in fridge:
            price += i["price"] # vlastni vypocet celkove ceny
        self.assertEqual(price, get_price()) # srovnani vracene a vypocitane hodnoty

    def test_get_count(self):
        fridge2 = []
        for i in fridge:
            if i in fridge2:
                fridge.remove(i)
            fridge2.append(i)
        count = len(fridge) # odstraneni duplicit + vlastni vypocet poctu prvku
        self.assertEqual(count, get_count()) # srovnani vracene a vypocitane hodnoty


    def test_get_average_price(self):
        fridge2 = []
        for i in fridge:
            if i in fridge2:
                fridge.remove(i)
            fridge2.append(i)
        count = len(fridge)  # odstraneni duplicit + vlastni vypocet poctu prvku
        price = 0
        for i in fridge:
            price += i["price"]  # vlastni vypocet celkove ceny
        self.assertEqual(price/count, get_average_price()) # srovnani vracene a vypocitane hodnoty


    def test_remove_expired(self):
        removed = []
        for i in fridge:
            if i["expired"]:
                removed.append(i) # vytvoreni seznamu s prvky, kde expired == True
        self.assertEqual(remove_expired(), removed) # porovnani vlastniho a vraceneho seznamu


    def test_set_expired(self):
        self.assertTrue(set_expired("egg")) # return True, u polozky, ktera je v seznamu
        for i in fridge:
            if i["name"] == "egg":
                index = fridge.index(i)
                break
        self.assertTrue(fridge[index]["expired"]) # test, zda byla zmenena hodnota expired na True
        self.assertEqual(set_expired("poop"), "poop" in [i["name"] for i in fridge])  # return False/True, podle toho, zda je polozka v seznamu
