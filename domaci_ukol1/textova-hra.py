"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.
"""

obnos = 400
inventar = ["mobil", "cislo na Lindu"]
pozice = "nonstop"


def moznosti():
    print("Co udelas? a - zavolas taxi, b - pujdes na rozjezd, c - zavolas Linde")
    volba = input()
    if volba == "c":
        linda()
    elif volba == "b":
        if pozice == "doma":
            rozjezd("nonstop")
        else:
            rozjezd("domu")
    elif volba == "a":
        if pozice == "nonstop":
            taxi("domu")
        elif pozice == "doma":
            taxi("nonstop")
    else:
        print("Musis zadat prave jednu z moznosti 'a', 'b' a 'c'")
        moznosti()


def linda():
    if "mobil" in inventar and "cislo na Lindu" in inventar:
        print("Volas Linde. Monotonni zvuk vytaceni te uspava. Po minute ztracis trpelivost a vzdavas to. Linda asi spi.\n")
    elif "mobil" in inventar:
        print("Prohledavas kapsy, ale zjistil jsi, ze jsi ztratil listek s cislem.\n")
    else:
        print("Chystas se zavolat Linde, chces vytahnout mobil a v panice zjistujes, ze ti ho grazlove museli ukradnout.\n")
    moznosti()


def rozjezd(cil):
    global obnos
    global pozice
    if cil == "domu":
        if "mobil" in inventar:
            print("Vydal ses na rozjezd. Potacis se opustenymi ulickami, cas od casu narazis do nejake popelnice a povalis\n"
                  "ji na zem. Pripada ti, ze skoro bezis, potrebujes se vydychat. Zastavis se na rohu dvou ulic, opres se \n"
                  "o omitku domu a v tom te prekvapi skupinka grazlu. Branit se nemas silu, navic maji noze. Dostanes par\n"
                  "pesti, svalis se na zem, kde ti na rozloucenou ustedri jeste par kopancu. Bolest ani necitis, na to mas\n"
                  "prilis otupele smysly. Vstanes a zpusobem ode zdi ke zdi konecne dorazis na zastavku. Nastoupis do preplneneho\n"
                  "rozjezdu, svalis se na posledni prazdnou sedacku a jedes. Vystoupis na sve zastavce a dojdes k domu.\n")
            inventar.remove("mobil")
            obnos = 0
            if "cislo na Lindu" in inventar:
                inventar.remove("cislo na Lindu")
            pozice = "doma"
            doma()
        else:
            print("Znovu prochazis ty zname ulice mezi nonstopem a zastavkou, ktere se mezitim zacaly plnit lidmi popsichajicimi\n"
                  "do prace. Divaji se na tebe jako na zjeveni. Az ted sis vsiml, ze jsi cely od krve a obleceni mas roztrhane.\n"
                  "Ale neda se nic delat... Dojdes na zastavku a pockas na rozjezd. Nez nastoupis, pro jistotu si zkontrolujes,\n"
                  "ze mas klice. Pak uz zase jedes. Vystoupis a dojdes k domovnim dverim.\n")
            doma()
    elif cil == "nonstop":
        print("Dosel jsi na zastavku, kde musis dlouho cekat na rozjezd. Zacina ti byt poradna zima. Nakonec se konecne dockas,\n"
              "nasedas a jedes zase zpatky do mesta. Zaspis spravnou zastavku, takze musis jit jeste pekny kus cesty pesky.\n")
        if "cislo na Lindu" in inventar:
            inventar.remove("cislo na Lindu")
        pozice = "nonstop"
        nonstop()


def taxi(cil):
    global obnos
    global pozice
    if obnos >= 350:
        if "mobil" in inventar:
            obnos -= 350
            if cil == "domu":
                print("Zavolal jsi taxi. Deset minut na nej cekas, potom nasedas a jedes domu, kde te taxikar vysazuje a odjizdi.\n")
                pozice = "doma"
                doma()
            elif cil == "nonstop":
                pozice = "nonstop"
                print("Zavolal jsi taxi. Deset minut na nej cekas, potom nasedas a jedes zpatky k nonstopu, kde te\n"
                      "taxikar vysazuje a odjizdi.\n")
                nonstop()
        else:
            print("Chtel jsi zavolat taxi, ale zjistil jsi, ze nemas jak, protoze ti grazlove ukradli mobil.\n")
            moznosti()
    else:
        print("Chystas se zavolat taxi, ale zjistujes, ze nemas dost penez.\n")
        moznosti()


def doma():
    if "klice" in inventar:
        print("Odemykas dvere a konecne jsi doma! Vyhral jsi!")
    else:
        print("Chystas se vejit dovnitr a padnout do postele, ale zjistujes, ze jsi asi v nonstopu zapomnel klice.\n"
              "Zkousis zvonit, pak si ale uvedomis, ze zijes sam a nema ti kdo otevrit.\n")
        moznosti()


def nonstop():
    print("A jsi zpatky v nonstopu! Rucicky hodin se mezitim znatelne posunuly, denni svetlo te pali v ocich. Ospaly\n"
          "vrchni vzhledne od baru, zasklebi se na tebe a hlavou pokyne ke stolu, na kterem se blysti tvoje klice.\n"
          "S ulevou je seberes a vyjdes znovu na ulici.\n")
    inventar.append("klice")
    moznosti()


print("Ze sna te probudi vypracovana ruka neohleduplne cloumajici tvymi rameny. Otevres oci, zamzouras a pred sebou spatris\n"
      "stul s asi osmi vyprazdnenymi pullitry. Zaostris. Krygle pomalu splynou do jednoho. Rozhlidnes se po mistnosti.\n"
      "U okolnich stolu je prazdno; oknem dovnitr pronika slabe svetlo, hodiny ukazuji 4:20, zacina svitat.\n"
      "Zvednes se, rozloucis se s vrchnim a vyjdes na ulici. Po kapsach nahmatas mobil, penezenku a pomackany listek\n"
      "s telefonim cislem a napisem 'Linda'. Kde se tam vzal, pripadne o koho jde, ti pamet odmita prozradit. Je\n"
      "cas vydat se domu.\n")

moznosti()
