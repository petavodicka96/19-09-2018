"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

bank = 1000000
rate = 19.9
money = 30000

letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
letter2 = """Na vanoce bich moc chtel -plejstejsn"""
letter3 = """Na vanoce bych si prala -ponika"""

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]


def getPresentFromLetter(letter, present_list):
    last_dash = -1
    for char in letter:
        if char == '-':
            last_dash = letter.index(char)
    present_list.append(letter[last_dash + 1:])
    return present_list


def findPresentsInEshop(present_list, stores):
    presents_to_buy = []
    for present in present_list:
        cheapest = {'present': present, 'shop': '', 'price': 100000000}
        for store in stores:
            for product in store['products']:
                if product['name'] == present and product['price'] < cheapest['price']:
                    cheapest['shop'] = store['name']
                    cheapest['price'] = product['price']
                    presents_to_buy.append(cheapest)

    return presents_to_buy


def getAllPresentPrice(presents_to_buy):
    price = 0
    for present in presents_to_buy:
        price += present['price']
    return price


def loanMoney(money, bank, price):
    if price > money:
        loan = price - money
        bank -= loan
        money += loan

    return money, bank, loan


def buyPresent(presents_to_buy, stores, money):
    for present in presents_to_buy:
        money -= present['price']
        for store in stores:
            if store['name'] == present['shop']:
                for product in store['products']:
                    if product['name'] == present['present']:
                        product['no_items'] -= 1
                        continue
            continue
    return money


def consoleOutput(money, loan, rate, presents_to_buy):
    print('Nakoupene darky:')
    for present in presents_to_buy:
        print('\tDarek: {}, obchod: {}, cena: {}'.format(present['present'], present['shop'], present['price']))
    print('Jeziskuv zustatek: {}'.format(money))
    print('Pujcka v bance: {}'.format(loan))
    print('Dluh po zuroceni: {}'.format(loan * (1 + rate / 100)))


present_list = []
getPresentFromLetter(letter1, present_list)
getPresentFromLetter(letter2, present_list)
getPresentFromLetter(letter3, present_list)

opraveny_seznam = ['hodinky s vodotryskem', 'playstation', 'ponik']

present_to_buy = findPresentsInEshop(opraveny_seznam, stores)

total = getAllPresentPrice(present_to_buy)

money, bank, loan = loanMoney(money, bank, total)
money = buyPresent(present_to_buy, stores, money)

consoleOutput(money, loan, rate, present_to_buy)
